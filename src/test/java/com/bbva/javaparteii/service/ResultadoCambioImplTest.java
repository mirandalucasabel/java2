package com.bbva.javaparteii.service;

import com.bbva.javaparteii.Service.ResultadoCambio;
import com.bbva.javaparteii.Service.impl.ResultadoCambioImpl;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class ResultadoCambioImplTest {

    @Test
    public void SetTasaTest() {
        ResultadoCambio resultadoCambio = new ResultadoCambioImpl();
        resultadoCambio.setTasa(0.05);
        assertEquals(0.05, ((ResultadoCambioImpl) resultadoCambio).Tasa, 0.001);
        //0.001 es la tolerancia
    }

    @Test
    public void SetResultadoTest() {
        ResultadoCambio resultadoCambio = new ResultadoCambioImpl();
        resultadoCambio.setResultado(100.0);
        assertEquals(100.0, ((ResultadoCambioImpl) resultadoCambio).resultado, 0.001);
    }


    @Test
    public void GetTasatest() {
        ResultadoCambio resultadoCambio = new ResultadoCambioImpl();
        resultadoCambio.setTasa(0.05);
        assertEquals(0.05, resultadoCambio.getTasa(), 0.001);
    }

    @Test
    public void GetResultadotest() {
        ResultadoCambio resultadoCambio = new ResultadoCambioImpl();
        resultadoCambio.setResultado(100.0);
        assertEquals(100.0, resultadoCambio.getResultado(), 0.001);
    }

}
