package com.bbva.javaparteii.service;

import com.bbva.javaparteii.Service.CambioServicio;
import com.bbva.javaparteii.Service.ResultadoCambio;
import com.bbva.javaparteii.Service.impl.CambioServicioImpl;
import com.bbva.javaparteii.util.currencyconverter.ApiResponse;
import com.bbva.javaparteii.util.currencyconverter.CurrencyConverter;
import com.bbva.javaparteii.util.currencyconverter.Info;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
public class CambioServicioImplTest {

    @Mock
    private CurrencyConverter currencyConverter;

    @InjectMocks
    private CambioServicioImpl cambioServicio;



    //falta mockear el currencyconverter
    //401 Unauthorized: "{"message":"Invalid authentication credentials"}"
    @Test
    public void testConvertCurrency() {
        // Given
        String sourceCurrency = "USD";
        String targetCurrency = "EUR";
        Double amount = 100.0;
        ApiResponse apiResponse = new ApiResponse();

        // **Mock the CurrencyConverter object**
        CurrencyConverter currencyConverter = Mockito.mock(CurrencyConverter.class);
        when(currencyConverter.getCurrencyChangeAmount(eq(sourceCurrency), eq(targetCurrency), eq(amount)))
                .thenReturn(apiResponse);

        // When
        CambioServicio cambioServicio = new CambioServicioImpl();
        ResultadoCambio result = cambioServicio.cambiar(sourceCurrency, targetCurrency, amount);

        // Then
        verify(currencyConverter, times(1)).getCurrencyChangeAmount(eq(sourceCurrency), eq(targetCurrency), eq(amount));

        // Assert

    }
}