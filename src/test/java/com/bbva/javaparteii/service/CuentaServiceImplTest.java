package com.bbva.javaparteii.service;

import com.bbva.javaparteii.Service.ClienteService;
import com.bbva.javaparteii.Service.impl.ClienteServiceImpl;
import com.bbva.javaparteii.Service.impl.CuentaServiceimpl;
import com.bbva.javaparteii.dao.ClienteRepository;
import com.bbva.javaparteii.dao.CuentaDao;
import com.bbva.javaparteii.model.Cliente;
import com.bbva.javaparteii.model.Cuenta;
import com.bbva.javaparteii.rest.request.CrearCuentaRequest;
import com.bbva.javaparteii.rest.response.ClienteResponse;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import javax.persistence.EntityNotFoundException;
import javax.validation.constraints.AssertTrue;
import java.math.BigDecimal;
import java.util.*;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
public class CuentaServiceImplTest {

    @Mock
    private CuentaDao clienteRepository;

    @Mock
    private Validator validator;

    @InjectMocks
    private CuentaServiceimpl cuentaService;



    @Test
    public void testCrearCuenta() {
        // Arrange
        Cuenta cuenta = new Cuenta();
        cuenta.setNumero(123456789);
        cuenta.setMoneda("ARS");
        cuenta.setSaldoInicial(new BigDecimal("100.00"));
        cuenta.setSaldoActual(new BigDecimal("100.00"));

        CrearCuentaRequest request = new CrearCuentaRequest();

        request.setMoneda("ARS");
        request.setSaldoInicial(new BigDecimal(100));
        request.setTitularId(1L);



        when(cuentaService.guardarCuenta(request)).thenReturn(cuenta);

        // Act
        cuentaService.guardarCuenta(request);

        // Assert
        assertThat(cuenta.getId()).isNotNull();
        assertThat(cuenta.getNumero()).isEqualTo(123456789);
        assertThat(cuenta.getMoneda()).isEqualTo("ARS");
        assertThat(cuenta.getSaldoInicial()).isEqualTo(new BigDecimal("100.00"));
        assertThat(cuenta.getSaldoActual()).isEqualTo(new BigDecimal("100.00"));
    }
}
