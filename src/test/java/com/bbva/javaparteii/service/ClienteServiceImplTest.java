package com.bbva.javaparteii.service;

import com.bbva.javaparteii.Service.ClienteService;
import com.bbva.javaparteii.Service.impl.ClienteServiceImpl;
import com.bbva.javaparteii.dao.ClienteRepository;
import com.bbva.javaparteii.model.Cliente;
import com.bbva.javaparteii.rest.response.ClienteResponse;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import javax.persistence.EntityNotFoundException;
import javax.validation.constraints.AssertTrue;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class ClienteServiceImplTest {

    @Mock
    private ClienteRepository clienteRepository;

    @Mock
    private Validator validator;

    @InjectMocks
    private ClienteServiceImpl clienteService;

    @Test
    void testGuardarClienteWithValidationErrors() {
        Cliente cliente = new Cliente();
        Errors errors = new BeanPropertyBindingResult(cliente, "cliente");
        errors.reject("error.code", "Error message");

        when(validator.supports(any())).thenReturn(true);
        doNothing().when(validator).validate(any(), any());

        ClienteResponse response = clienteService.guardarCliente(cliente);



        verify(clienteRepository, never()).save(any());

        assertTrue(response.getMensajes().contains("Error message"));
    }

    @Test
    void GuardarClienteSinValidacionErrorstest() {
        Cliente cliente = new Cliente();
        Errors errors = new BeanPropertyBindingResult(cliente, "cliente");

        when(validator.supports(any())).thenReturn(true);
        doNothing().when(validator).validate(any(), any());

        ClienteResponse response = clienteService.guardarCliente(cliente);


        verify(clienteRepository, times(1)).save(cliente);

        assertEquals(cliente, response.getCliente());
    }

    @Test
    void ObtenerClienteExistentePorIdtest() {
        Long id = 1L;
        Cliente cliente = new Cliente();
        when(clienteRepository.getById(id)).thenReturn(cliente);

        Cliente result = clienteService.obtenerClientePorId(id);

        assertEquals(cliente, result);
        verify(clienteRepository, times(1)).getById(id);
    }

    @Test
    void testObtenerClienteNoExistentePorId() {
        Long id = 2L;
        when(clienteRepository.getById(id)).thenThrow(new EntityNotFoundException("Cliente no encontrado"));

        assertThrows(EntityNotFoundException.class, () -> clienteService.obtenerClientePorId(id));
        verify(clienteRepository, times(1)).getById(id);
    }

    @Test
    void testObtenerTodosLosClientesConDatos() {

        List<Cliente> clientes = Arrays.asList(new Cliente(), new Cliente(), new Cliente());
        when(clienteRepository.findAll()).thenReturn(clientes);

        List<Cliente> result = clienteService.obtenerTodosLosClientes();


        assertEquals(clientes, result);
        verify(clienteRepository, times(1)).findAll();
    }

    @Test
    void testObtenerTodosLosClientesSinDatos() {

        when(clienteRepository.findAll()).thenReturn(Arrays.asList());

        List<Cliente> result = clienteService.obtenerTodosLosClientes();


        assertTrue(result.isEmpty());
        verify(clienteRepository, times(1)).findAll();
    }

    @Test
    void testEliminarClienteExistente() {

        Cliente cliente = new Cliente();
        when(clienteRepository.existsById(anyLong())).thenReturn(true);


        clienteService.eliminarCliente(cliente);


        verify(clienteRepository, times(1)).delete(cliente);
    }



    @Test
    void ObtenerPorNombreSinResultadostest() {

        String nombre = "Cliente Inexistente";
        when(clienteRepository.findByNombre(nombre)).thenReturn(new HashSet<>());

        Set<Cliente> result = clienteService.obtenerPorNombre(nombre);


        assertTrue(result.isEmpty());
        verify(clienteRepository, times(1)).findByNombre(nombre);
    }




    @Test
    void buscarPorNombreTest() {

        String nombreEsperado = "Andres";

       Cliente esperado = new Cliente();
       esperado.setNombre(nombreEsperado);
       Set<Cliente> listaEsperado = new HashSet<>();
       listaEsperado.add(esperado);


        when(clienteService.obtenerPorNombre (nombreEsperado)).thenReturn(listaEsperado);

        assertTrue(clienteService.obtenerPorNombre(nombreEsperado).contains(esperado));

    }
}
