package com.bbva.javaparteii.service;

import com.bbva.javaparteii.Service.MovimientoService;
import com.bbva.javaparteii.Service.ResultadoCambio;
import com.bbva.javaparteii.Service.impl.ClienteServiceImpl;
import com.bbva.javaparteii.Service.impl.MovimientoServiceimpl;
import com.bbva.javaparteii.dao.CuentaDao;
import com.bbva.javaparteii.dao.MovimientoDao;
import com.bbva.javaparteii.model.Cuenta;
import com.bbva.javaparteii.model.Movimiento;
import com.bbva.javaparteii.model.movimiento.CompraMonedaExtranjera;
import com.bbva.javaparteii.model.movimiento.TransferenciaRealizada;
import com.bbva.javaparteii.model.movimiento.TransferenciaRecibida;
import com.bbva.javaparteii.model.movimiento.VentaMonedaExtranjera;
import com.bbva.javaparteii.rest.request.TransferenciaRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class MovimientoServiceTest {

    @InjectMocks
    private MovimientoServiceimpl movimientoService;

    @Mock
    private CuentaDao cuentaRepository;

    @Mock
    private MovimientoDao movimientoDao;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }
    @Test
    public void testGuardarMovimiento() {


    }

    @Test
    public void testObtenerMovimientoPorId() {
        //TODO completar test
    }

    @Test
    public void testObtenerTodosLosMovimientos() {
        //TODO completar test
    }

    @Test
    public void testEliminarMovimiento() {
        //TODO esto no fue implementado y probado en el controller
    }

    @Test
    public void testObtenerTodosLosMovimientosPorIdTitular() {
        //TODO completar test
    }

    @Test
    public void testObtenerTodosLosMovimientosPorIdCoTitular() {

//TODO completar test


    }

    @Test
    public void RealizarTransferenciaTest() {

        Cuenta cuentaOrigen = new Cuenta();
        cuentaOrigen.setId(1L);
        cuentaOrigen.setSaldoActual(new BigDecimal(100));


        Cuenta cuentaDestino = new Cuenta();
        cuentaDestino.setId(1L);
        cuentaDestino.setSaldoActual(new BigDecimal(100));


        when(cuentaRepository.getById(1L)).thenReturn(cuentaOrigen);

        when(cuentaRepository.getById(2L)).thenReturn(cuentaDestino);


        TransferenciaRequest request = new TransferenciaRequest();
        request.setMonto(new BigDecimal(100));
        request.setIdCuentaOrigen(1L);
        request.setIdCuentaDestino(2L);

        when(movimientoDao.save(any())).thenReturn(new TransferenciaRealizada());


        Movimiento resultado = movimientoService.realizarTransferencia(request);

        assertEquals(resultado.getMonto(),request.getMonto());


    }
}
