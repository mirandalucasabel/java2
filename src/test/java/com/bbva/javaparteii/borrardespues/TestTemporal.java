package com.bbva.javaparteii.borrardespues;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class TestTemporal {

    private EntityManagerFactory emf;
    private EntityManager em;

    @Before
    public void iniciar() {
        emf = Persistence.createEntityManagerFactory("banquito");
        em = emf.createEntityManager();
    }

    @After
    public void cerrar() {
        em.close();
        emf.close();
    }

    @Test
    public void testAgregarDatos() {
        em.getTransaction().begin();

        // Crear clientes
        /*Cliente cliente1 = new Cliente("John",
                "Doe",
                new Direccion("Apt 4B", "Departamento 123", "Piso 3", "Ciudad", "12345", "prov ej","Tierra del fuego"),
                "4123-1234",
                "lucas@mail.com.ar");
        Cliente cliente2 = new Cliente("Jane", "Smith",
                new Direccion("Apt 4B", "Departamento 2", "Piso 3", "otra ciudad", "12345", "prov ej","buenos aires"),

                "1234-5678",
                "juan@mail.com");

        // Crear cuentas
        Cuenta cuenta1 = new Cuenta(1001L, "USD", cliente1, 1000.0, 500.0, "2023-12-31");
        Cuenta cuenta2 = new Cuenta(2002L, "ARS", cliente2, 800.0, 400.0, "2023-12-31");

        //TODO REVISAR
        // Agregar cotitulares
        //cliente1.agregarCuenta(cuenta1);
        //cliente2.agregarCuenta(cuenta2);

        // Realizar movimientos
        Movimiento movimiento1 = new Movimiento("2023-10-27 14:30:00", 200.0, "dep");
        Movimiento movimiento2 = new Movimiento("2023-10-28 10:15:00", -100.0, "Retiro");

        //cuenta1.realizarMovimiento(movimiento1); //TODO revisar
        //cuenta1.realizarMovimiento(movimiento2);

        em.persist(cliente1);
        em.persist(cliente2);

        em.getTransaction().commit();
        */

    }
}
