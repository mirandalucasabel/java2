package com.bbva.javaparteii.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.ConstraintViolation;
import java.util.Set;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class ClienteTest {

    private Validator validator;

    @BeforeEach
    public void config() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void clienteValidoTest() {
        Cliente cliente = new Cliente();
        cliente.setNombre("lucas");
        cliente.setApellido("martinez");
        cliente.setEmail("mail@mails.com");
        cliente.setTelefono("12341234");

        Set<ConstraintViolation<Cliente>> violations = validator.validate(cliente);
        assertTrue(violations.isEmpty());
    }

    @Test
    public void clienteInvalidoTest() {
        Cliente cliente = new Cliente(); // Cliente sin nombre ni apellido

        Set<ConstraintViolation<Cliente>> violations = validator.validate(cliente);
        assertFalse(violations.isEmpty());
    }
}
