package com.bbva.javaparteii.model;

import com.bbva.javaparteii.Service.impl.CuentaServiceimpl;
import com.bbva.javaparteii.dao.CuentaDao;
import com.bbva.javaparteii.model.movimiento.CompraMonedaExtranjera;
import com.bbva.javaparteii.rest.request.CrearCuentaRequest;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@SpringBootTest
public class CuentaTest {

    @Mock
    private Cliente cliente;

    @InjectMocks
    private CuentaServiceimpl cuentaServiceimpl;

    @Test
    public void testCrearCuenta() {
        // Arrange
        Cuenta cuenta = new Cuenta();
        cuenta.setNumero(123456789);
        cuenta.setMoneda("ARS");
        cuenta.setSaldoInicial(new BigDecimal("100.00"));
        cuenta.setSaldoActual(new BigDecimal("100.00"));

        CrearCuentaRequest request = new CrearCuentaRequest();

        request.setMoneda("ARS");
        request.setSaldoInicial(new BigDecimal(100));
        request.setTitularId(1L);


        when(cuentaServiceimpl.guardarCuenta(request)).thenReturn(cuenta);

        // Act
        cuentaServiceimpl.guardarCuenta(request);

        // Assert
        assertThat(cuenta.getId()).isNotNull();
        assertThat(cuenta.getNumero()).isEqualTo(123456789);
        assertThat(cuenta.getMoneda()).isEqualTo("ARS");
        assertThat(cuenta.getSaldoInicial()).isEqualTo(new BigDecimal("100.00"));
        assertThat(cuenta.getSaldoActual()).isEqualTo(new BigDecimal("100.00"));
    }

    @Test
    public void testAgregarTitular() {
        // Arrange
        Cuenta cuenta = new Cuenta();
        cuenta.setNumero(123456789);
        cuenta.setMoneda("ARS");
        cuenta.setSaldoInicial(new BigDecimal("100.00"));
        cuenta.setSaldoActual(new BigDecimal("100.00"));

        cliente.setNombre("Juan Pérez");
        cliente.setApellido("Pérez");

        // Act
        cuenta.agregarTitular(cliente);

        // Assert
       // assertThat(cuenta.getTitulares()).contains(cliente);
        //assertThat(cliente.getCuentasTitular()).contains(cuenta);
    }

    @Test
    public void testAgregarMovimiento() {
        // Arrange
        Cuenta cuenta = new Cuenta();
        cuenta.setNumero(123456789);
        cuenta.setMoneda("ARS");
        cuenta.setSaldoInicial(new BigDecimal("100.00"));
        cuenta.setSaldoActual(new BigDecimal("100.00"));

        Movimiento movimiento = new CompraMonedaExtranjera();
        movimiento.setFechaHora(new java.sql.Date(System.currentTimeMillis()));
        movimiento.setMonto(new BigDecimal("100.00"));
        movimiento.setDescripcion("venta de dolares");

        // Act
        cuenta.agregarMovimiento(movimiento);

        // Assert
        //Esto deberian ser 2 tests, que movimiento no sea null y que tampoco este vacio
        assertThat(cuenta.getMovimientos()).isNotNull();
        assertFalse(cuenta.getMovimientos().isEmpty());
    }
}
