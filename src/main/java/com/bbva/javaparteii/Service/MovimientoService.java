package com.bbva.javaparteii.Service;
import com.bbva.javaparteii.model.Movimiento;
import com.bbva.javaparteii.rest.request.TransferenciaRequest;


import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

public interface MovimientoService {

        Movimiento guardarMovimiento(Movimiento Movimiento);
        Movimiento obtenerMovimientoPorId(Long id);
        Set<Movimiento> obtenerTodosLosMovimientos();
        void eliminarMovimiento(Movimiento cliente);

        Set<Movimiento> obtenerTodosLosMovimientosPorIdTitular();

        Set<Movimiento> obtenerTodosLosMovimientosPorIdCoTitular();


    Movimiento realizarTransferencia(TransferenciaRequest request);
}
