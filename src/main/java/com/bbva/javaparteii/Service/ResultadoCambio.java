package com.bbva.javaparteii.Service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


public interface ResultadoCambio {
    /**
     * @return Tasa aplicada al cambio
     */
    public Double getTasa();
    /**
     * @return El resultado de la conversion
     */
    public Double getResultado();


    public void setTasa(Double tasa);

    public void setResultado(Double resultado);

}
