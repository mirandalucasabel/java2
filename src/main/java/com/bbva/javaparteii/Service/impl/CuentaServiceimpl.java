package com.bbva.javaparteii.Service.impl;

import com.bbva.javaparteii.Service.CambioServicio;
import com.bbva.javaparteii.Service.CuentaService;
import com.bbva.javaparteii.Service.MovimientoService;
import com.bbva.javaparteii.Service.ResultadoCambio;
import com.bbva.javaparteii.dao.ClienteDao;
import com.bbva.javaparteii.dao.CuentaDao;
import com.bbva.javaparteii.dao.MovimientoDao;
import com.bbva.javaparteii.model.Cliente;
import com.bbva.javaparteii.model.Cuenta;

import com.bbva.javaparteii.model.Movimiento;
import com.bbva.javaparteii.model.movimiento.CompraMonedaExtranjera;
import com.bbva.javaparteii.model.movimiento.VentaMonedaExtranjera;
import com.bbva.javaparteii.rest.request.AgregarCotitularRequest;
import com.bbva.javaparteii.rest.request.CrearCuentaRequest;
import com.bbva.javaparteii.rest.request.CurrencyRequest;
import org.hibernate.cfg.NotYetImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Set;

@Service
public class CuentaServiceimpl implements CuentaService {

    private final CuentaDao cuentaRepository;

    private final ClienteDao clienteDao; //TODO MIGRAR AL REPO JPA

    private final com.bbva.javaparteii.dao.MovimientoDao movimientoDao;


    private final CambioServicio cambioServicio;

    private final MovimientoService movimientoService;

    @Autowired
    public CuentaServiceimpl(CuentaDao cuentaRepository, ClienteDao clienteDao, MovimientoDao movimientoDao, CambioServicio cambioServicio, MovimientoService movimientoService) {
        this.cuentaRepository = cuentaRepository;
        this.clienteDao = clienteDao;
        this.movimientoDao = movimientoDao;
        this.cambioServicio = cambioServicio;
        this.movimientoService = movimientoService;
    }

    @Override
    public Cuenta guardarCuenta(CrearCuentaRequest request) {
        Cuenta cuenta = new Cuenta();
        cuenta.setSaldoInicial(request.getSaldoInicial());
        cuenta.setSaldoActual(request.getSaldoInicial());
        cuenta.setMoneda(request.getMoneda());
        cuenta.agregarTitular(clienteDao.getById(request.getTitularId()));
        return cuentaRepository.save(cuenta);
    }

    @Override
    public Cuenta obtenerCuentaPorId(Long id) {
        return cuentaRepository.findById(id).orElse(null);
    }

    @Override
    public Set<Cuenta> obtenerTodasLasCuentas() {
        return (Set<Cuenta>) cuentaRepository.findAll(); //TODO no estoy seguro de que esto este bien, resolver despues
    }

    @Override
    public void eliminarCuenta(Cuenta cuenta) {
        cuentaRepository.delete(cuenta);
    }

    @Override
    public void hacerTransferencia(Long idCuentaOrigen, Long idCuentaDestino, Long monto) {
        Cuenta cuentaOrigen = obtenerCuentaPorId(idCuentaOrigen);
        Cuenta cuentaDestino = obtenerCuentaPorId(idCuentaDestino);

        throw new NotYetImplementedException("TODO implementar la transferencia");

    }

    @Override
    @Transactional
    public Movimiento vender(CurrencyRequest request) {


        System.out.println("paso 1");
        //TODO tirar esto a algun service para limpiar el controller
        Cuenta cuentaOrigen = obtenerCuentaPorId(request.getIdCuentaOrigen());
        Cuenta cuentaDestino = obtenerCuentaPorId(request.getIdCuentaDestino());

        String monedaOrigen = cuentaOrigen.getMoneda();
        String monedaDestino = cuentaDestino.getMoneda();

        Double monto = request.getMonto();

        ResultadoCambio responseCambio = cambioServicio.cambiar(monedaOrigen,monedaDestino,monto);

        System.out.println("paso 2");
        //cuenta origen
        BigDecimal resultadoOperacionOrigen = cuentaOrigen.getSaldoActual();
        resultadoOperacionOrigen = resultadoOperacionOrigen.subtract( new BigDecimal(monto));
        cuentaOrigen.setSaldoActual(resultadoOperacionOrigen);

        //cuenta Destino
        BigDecimal resultadoOperacionDestino = cuentaDestino.getSaldoActual();
        resultadoOperacionDestino = resultadoOperacionOrigen.add( new BigDecimal(responseCambio.getResultado()));
        cuentaDestino.setSaldoActual(resultadoOperacionDestino);

        Movimiento venta = new VentaMonedaExtranjera();
        Movimiento resultado = new CompraMonedaExtranjera();

        ///venta.setCuentasTitular(cuentaOrigen.getTitulares());
        venta.setMonto(resultadoOperacionOrigen);

        //resultado.setCuentasTitular(cuentaDestino.getTitulares());
        resultado.setMonto(resultadoOperacionDestino);

        movimientoDao.save(venta);
        movimientoDao.save(resultado);

        cuentaOrigen.agregarMovimiento(venta);
        cuentaDestino.agregarMovimiento(venta);


        actualizar(cuentaDestino);
        actualizar(cuentaOrigen);

        System.out.println("paso 3");



        return venta;

    }

    @Override
    public Cuenta actualizar(Cuenta cuenta) {
        return cuentaRepository.save(cuenta);
    }

    @Override
    public Cuenta agregarCotitular(AgregarCotitularRequest request) {
        Cuenta cuenta = cuentaRepository.getById(request.getIdCuenta());
        Cliente cotitular = clienteDao.getById(request.getIdCotitular());

        //TODO AGREGAR VALIDACIONES
        /*
        Agregar a un cliente como cotitular de una cuenta, dadas la id del cliente y la
        id de la cuenta. La operación no se puede realizar si el cliente ya figura como
        titular o cotitular de la cuenta ni si la cuenta está cerrada
         */


        //TODO casi seguro que esto se puede limpiar un toque mas pero mas o menos funciona
        Set<Cliente>cotitularesActuales =  cuenta.getCotitulares();
        cotitularesActuales.add(cotitular);
        cuenta.setCotitulares(cotitularesActuales);
        cuenta = cuentaRepository.save(cuenta);

        Set<Cuenta> cuentasCotitulares = cotitular.getCuentasCotitular();
        cuentasCotitulares.add(cuenta);
        cotitular.setCuentasCotitular(cuentasCotitulares);
        clienteDao.save(cotitular);

        clienteDao.save(cotitular);

        return cuenta;
    }
}
