package com.bbva.javaparteii.Service.impl;

import com.bbva.javaparteii.Service.ClienteService;
import com.bbva.javaparteii.dao.ClienteDao;
import com.bbva.javaparteii.dao.ClienteRepository;
import com.bbva.javaparteii.model.Cliente;
import com.bbva.javaparteii.rest.response.ClienteResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;


import java.util.List;
import java.util.Set;

@Service
public class ClienteServiceImpl implements ClienteService {

    private final ClienteRepository clienteRepository;
    private final Validator validator;

    @Autowired
    public ClienteServiceImpl(ClienteRepository clienteRepository, Validator validator) {

        this.clienteRepository = clienteRepository;
        this.validator = validator;
    }

    @Override
    @Transactional
    public ClienteResponse guardarCliente(Cliente cliente) {

        ClienteResponse response = new ClienteResponse();

        Errors errors = new BeanPropertyBindingResult(cliente, "cliente");

        ValidationUtils.invokeValidator(validator, cliente, errors);


        if (errors.hasErrors()) {
            //TODO CREAR UN RESPONSE PARA DEVOLVER LA INFO DE LOS ERRORES
            System.out.println("Errores de validación:");
            errors.getAllErrors().forEach(error -> {
                response.getMensajes().add(error.getDefaultMessage());
            });
        } else {
            // Si no hay errores, persiste el cliente
            clienteRepository.save(cliente);
            response.setCliente(cliente);
        }



        return response;
    }

    @Override
    public Cliente obtenerClientePorId(Long id) {
        return clienteRepository.getById(id);
    }

    @Override
    public List<Cliente> obtenerTodosLosClientes() {
        return clienteRepository.findAll();
    }

    @Override
    public void eliminarCliente(Cliente cliente) {
        clienteRepository.delete(cliente);
    }

    @Override
    public Set<Cliente> obtenerPorNombre(String nombre) {
        return clienteRepository.findByNombre(nombre);
    }
}
