package com.bbva.javaparteii.Service.impl;

import com.bbva.javaparteii.Service.ResultadoCambio;
import lombok.*;

@Data
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ResultadoCambioImpl implements ResultadoCambio {

    public Double Tasa;
    public Double resultado;


    @Override
    public void setTasa(Double Tasa) {
         this.Tasa = Tasa;
    }

    @Override
    public void setResultado(Double resultado) {
         this.resultado = resultado;
    }
}
