package com.bbva.javaparteii.Service.impl;

import com.bbva.javaparteii.Service.CambioServicio;
import com.bbva.javaparteii.Service.ClienteService;
import com.bbva.javaparteii.Service.ResultadoCambio;
import com.bbva.javaparteii.util.currencyconverter.ApiResponse;
import com.bbva.javaparteii.util.currencyconverter.CurrencyConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
@Service
public class CambioServicioImpl implements CambioServicio {

    @Override
    public ResultadoCambio cambiar(String monedaOrigen, String monedaDestino, Double monto) {


        //TODO evitar el new
        CurrencyConverter currencyConverter = new CurrencyConverter();

        //TODO falta el autowired
        ResultadoCambio resultadoCambio = new ResultadoCambioImpl();

        ApiResponse apiResponse =  currencyConverter.getCurrencyChangeAmount(monedaOrigen,monedaDestino, monto);

         resultadoCambio.setResultado(apiResponse.getResult());

         resultadoCambio.setTasa(apiResponse.getInfo().getQuote());

        return resultadoCambio;
    }
}
