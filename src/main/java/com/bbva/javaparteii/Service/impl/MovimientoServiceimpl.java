package com.bbva.javaparteii.Service.impl;

import com.bbva.javaparteii.Service.MovimientoService;
import com.bbva.javaparteii.dao.CuentaDao;
import com.bbva.javaparteii.dao.MovimientoDao;
import com.bbva.javaparteii.model.Cuenta;
import com.bbva.javaparteii.model.Movimiento;
import com.bbva.javaparteii.model.movimiento.TransferenciaRealizada;
import com.bbva.javaparteii.model.movimiento.TransferenciaRecibida;
import com.bbva.javaparteii.rest.request.TransferenciaRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Set;

@Service
public class MovimientoServiceimpl implements MovimientoService {

    private final CuentaDao cuentaRepository;

    private final com.bbva.javaparteii.dao.MovimientoDao movimientoDao;

    @Autowired
    public MovimientoServiceimpl(CuentaDao cuentaRepository, MovimientoDao movimientoDao) {
        this.cuentaRepository = cuentaRepository;
        this.movimientoDao = movimientoDao;
    }


    @Override
    public Movimiento guardarMovimiento(Movimiento Movimiento) {
        return null;
    }

    @Override
    public Movimiento obtenerMovimientoPorId(Long id) {
        return null;
    }

    @Override
    public Set<Movimiento> obtenerTodosLosMovimientos() {
        return null;
    }

    @Override
    public void eliminarMovimiento(Movimiento cliente) {

    }

    @Override
    public Set<Movimiento> obtenerTodosLosMovimientosPorIdTitular() {
        return null;
    }

    @Override
    public Set<Movimiento> obtenerTodosLosMovimientosPorIdCoTitular() {
        return null;
    }


    @Override
    @Transactional
    public Movimiento realizarTransferencia(TransferenciaRequest request) {

        Cuenta origen = cuentaRepository.getById(request.getIdCuentaOrigen());
        Cuenta destino = cuentaRepository.getById(request.getIdCuentaDestino());

        if(true) //|| origen.getSaldoActual() < request.getMonto().intValue()) //TODO ESTO ESTA MAL RESOLVER DESPUES
        {
            System.out.print("El saldo es insuficiente para realizar la transferencia");
            //return null;
        }

        origen.setSaldoActual(origen.getSaldoActual().subtract(request.getMonto())); //TODO ESTO ESTA MAL RESOLVER DESPUES
        destino.setSaldoActual(destino.getSaldoActual().add(request.getMonto())); //

        Movimiento transferenciaEnviada = new TransferenciaRealizada();
        Movimiento transferenciaRecibida = new TransferenciaRecibida();

        //TODO TERMINAR LAS ASIGNACIONES
        transferenciaEnviada.setMonto(request.getMonto());
        transferenciaRecibida.setMonto(request.getMonto());

        origen.agregarMovimiento(transferenciaEnviada);
        destino.agregarMovimiento(transferenciaRecibida);



        movimientoDao.save(transferenciaEnviada);
        movimientoDao.save(transferenciaRecibida);

        cuentaRepository.save(origen);
        cuentaRepository.save(destino);

        System.out.println("La operacion fue exitosa");

        return transferenciaEnviada;
    }

}
