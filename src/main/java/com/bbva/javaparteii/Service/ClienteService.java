package com.bbva.javaparteii.Service;

import com.bbva.javaparteii.model.Cliente;
import com.bbva.javaparteii.rest.response.ClienteResponse;

import java.util.List;
import java.util.Set;

public interface ClienteService {
    ClienteResponse guardarCliente(Cliente cliente);
    Cliente obtenerClientePorId(Long id);
    List<Cliente> obtenerTodosLosClientes();
    void eliminarCliente(Cliente cliente);

    Set<Cliente> obtenerPorNombre(String nombre);
}
