package com.bbva.javaparteii.Service;

import com.bbva.javaparteii.model.Cuenta;
import com.bbva.javaparteii.model.Movimiento;
import com.bbva.javaparteii.rest.request.AgregarCotitularRequest;
import com.bbva.javaparteii.rest.request.CrearCuentaRequest;
import com.bbva.javaparteii.rest.request.CurrencyRequest;

import java.util.Set;

public interface CuentaService {
    Cuenta guardarCuenta(CrearCuentaRequest request);
    Cuenta obtenerCuentaPorId(Long id);
    Set<Cuenta> obtenerTodasLasCuentas();
    void eliminarCuenta(Cuenta cuenta);

    //TODO revisar si monto debe ser un long o un big int, para mi es un big int
    void hacerTransferencia(Long idCuentaOrigen, Long idCuentaDestino, Long monto);

    Movimiento vender(CurrencyRequest request);

    Cuenta actualizar(Cuenta cuenta);

    Cuenta agregarCotitular(AgregarCotitularRequest request);
}
