package com.bbva.javaparteii.Service;

import java.math.BigDecimal;

public interface CambioServicio {
    /**
     * Permite convertir un monto de una moneda
     * a otra aplicando la tasa de conversion actual
     * @param de Moneda identificación de la moneda
     * del monto a convertir
     * @param a Moneda del monto convertido
     * @param monto a convertir
     * @return el resultado de la operación de cambio
     */
    //TODO cambiar el Object por Moneda, tambien crear la clase moneda
    public ResultadoCambio cambiar(String monedaOrigen, String monedaDestino, Double
            monto);
}
