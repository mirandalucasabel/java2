package com.bbva.javaparteii.model;

import com.fasterxml.jackson.annotation.JsonIgnore;



import javax.validation.ValidatorFactory;

import lombok.*;



import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;



    @NotNull(message = "El campo 'nombre' no puede ser nulo")
    @NotBlank(message = "El campo 'nombre' no puede estar en blanco")
    private String nombre;

    @NotNull(message = "El campo 'apellido' no puede ser nulo")
    @NotBlank(message = "El campo 'apellido' no puede estar en blanco")
    private String apellido;

    @Embedded
    private Direccion direccion;

    @NotNull(message = "El campo 'telefono' no puede ser nulo")
    @NotBlank(message = "El campo 'telefono' no puede estar en blanco")
    private String telefono;

    @NotNull(message = "El campo 'email' no puede ser nulo")
    @NotBlank(message = "El campo 'email' no puede estar en blanco")
    private String email;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToMany
    @JsonIgnore
    @JoinTable(
            name = "cuenta_cliente_titular",
            joinColumns = @JoinColumn(name = "cliente_id"),
            inverseJoinColumns = @JoinColumn(name = "cuenta_id")
    )
    private Set<Cuenta> cuentasTitular;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonIgnore
    @ManyToMany
    @JoinTable(
            name = "cuenta_cliente_cotitular",
            joinColumns = @JoinColumn(name = "cliente_id"),
            inverseJoinColumns = @JoinColumn(name = "cuenta_id")
    )

    private Set<Cuenta> cuentasCotitular;


}
