package com.bbva.javaparteii.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
//TODO revisar el tipo de estrategia para las tablas
@Inheritance(strategy = InheritanceType.SINGLE_TABLE) // Otra opción es TABLE_PER_CLASS
@DiscriminatorColumn(name = "tipo_movimiento", discriminatorType = DiscriminatorType.STRING)
public abstract class Movimiento {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //TODO revisar si el import era de DATE o SQL DATE, creo que era de SQL DATE
    private Date fechaHora;
    private BigDecimal monto;
    private String descripcion;


    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToMany(mappedBy = "cuentasTitular")
    private Set<Cliente> cuentasTitular = new HashSet<>();




}