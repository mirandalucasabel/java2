package com.bbva.javaparteii.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.hibernate.cfg.NotYetImplementedException;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Cuenta {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private long numero;

    @NotNull(message = "El campo 'moneda' no puede ser nulo")
    @NotBlank(message = "El campo 'moneda' no puede estar en blanco")
    //lo ideal seria que esto estuviera en otra tabla para que sea mas parametrizable
    @Pattern(regexp = "^(ARS|USD|EUR)$", message = "El campo 'moneda' debe ser 'ARS', 'USD' o 'EUR'")
    private String moneda;


    /*TODO revisar que validaciones son correctas para los bigdecimal
    @NotNull(message = "El campo 'saldoInicial' no puede ser nulo")
    @NotBlank(message = "El campo 'saldoInicial' no puede estar en blanco")
    */
    private BigDecimal saldoInicial;


    private BigDecimal saldoActual;



    //TODO averiguar que este campo
    private BigDecimal descubiertoAcordado;


    private String fechaCierre;




    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToMany(mappedBy = "cuentasTitular" ,fetch = FetchType.LAZY)
    private Set<Cliente> titulares = new HashSet<>();;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToMany(mappedBy = "cuentasCotitular",fetch = FetchType.LAZY)
    private Set<Cliente> cotitulares = new HashSet<>();;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToMany
    @JoinTable(
            name = "cuenta_movimiento",
            joinColumns = @JoinColumn(name = "cuenta_id"),
            inverseJoinColumns = @JoinColumn(name = "movimiento_id")
    )
    private Set<Movimiento> movimientos = new HashSet<>();


    public void agregarTitular(Cliente cliente) {
        titulares.add(cliente);
        cliente.getCuentasTitular().add(this);
    }

    public void agregarMovimiento(Movimiento movimiento) {
        movimientos.add(movimiento);
        //cliente.getCuentasTitular().add(this);
    }

}
