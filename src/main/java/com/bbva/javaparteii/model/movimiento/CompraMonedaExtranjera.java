package com.bbva.javaparteii.model.movimiento;

import com.bbva.javaparteii.model.Movimiento;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.math.BigDecimal;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@DiscriminatorValue("CompraMonedaExtranjera")
public class CompraMonedaExtranjera extends Movimiento {
    private Long idCuentaOrigen;
    private Long idCuentaDestino;
    private BigDecimal monto;

}
