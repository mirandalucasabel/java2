package com.bbva.javaparteii.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.Entity;


@Embeddable
public class Direccion {



    private String calle;
    private String numero;
    private String depto;
    private String piso;
    private String ciudad;
    private String codigoPostal;
    private String provincia;
    private String pais;


}
