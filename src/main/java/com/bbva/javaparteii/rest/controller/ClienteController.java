package com.bbva.javaparteii.rest.controller;

import com.bbva.javaparteii.Service.ClienteService;
import com.bbva.javaparteii.model.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

@RestController
public class ClienteController {

    private final ClienteService clienteService;
    @Autowired
    public ClienteController(ClienteService clienteService) {
        this.clienteService = clienteService;
    }

    //agregar un    cliente como cotitular de una cuenta
    @PostMapping("/agregarCotitular")
    public void agregarCotitular() {
        //return clienteService.obtenerTodosLosClientes();
        System.out.println("agregar cotitular");
    }

    //realizar una transferencia a una  cuenta del mismo banco
    @PostMapping("/transferir")
    //TODO crear un request para esto
    public void transferir(Long idCuentaOrigen, Long idCuenta, BigDecimal monto) {
        //return clienteService.obtenerTodosLosClientes();
        System.out.println("agregar cotitular");
    }

    //mostrar todos los movimientos

    @GetMapping("/movimientos/{id}")
    //TODO crear un request para esto
    public void movimientos(Long idCuenta) {
        System.out.println("mostrar todos los movimientos de una cuenta");
    }


    @GetMapping("/buscarPorNombre/{nombre}")
    public ResponseEntity<Set<Cliente>> eliminarCuenta(@PathVariable String nombre) {

        return ResponseEntity.ok(clienteService.obtenerPorNombre(nombre));
    }
}
