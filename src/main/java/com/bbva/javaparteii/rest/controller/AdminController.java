package com.bbva.javaparteii.rest.controller;

import com.bbva.javaparteii.Service.ClienteService;
import com.bbva.javaparteii.model.Cliente;
import com.bbva.javaparteii.model.Cuenta;
import com.bbva.javaparteii.rest.response.ClienteResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//TODO posiblemente esto deberia tener otro nombre
@RestController
public class AdminController {

    //TODO cambiar el tipo de return para devolver los estados http
    @Autowired
    public AdminController(ClienteService clienteService) {
        this.clienteService = clienteService;
    }

    private final ClienteService clienteService;


    @GetMapping("/usuarios")
    public List<Cliente> obtenerTodosLosClientes() {
        return clienteService.obtenerTodosLosClientes();
    }

    @GetMapping("/{id}")
    public Cliente obtenerClientePorId(@PathVariable Long id) {
        return clienteService.obtenerClientePorId(id);
    }

    @PostMapping(value = "/crearUsuario")
    public ClienteResponse crearCliente(@RequestBody Cliente cliente) {

        return clienteService.guardarCliente(cliente);
    }

    @PutMapping("/{id}")
    public ClienteResponse actualizarCliente(@PathVariable Long id, @RequestBody Cliente cliente) {
        return clienteService.guardarCliente(cliente);
    }

    @DeleteMapping("/{id}")
    public void eliminarCliente(@PathVariable Long id) {
        clienteService.eliminarCliente(obtenerClientePorId(id));
        return ;
    }

}
