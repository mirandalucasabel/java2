package com.bbva.javaparteii.rest.controller;

import com.bbva.javaparteii.Service.CambioServicio;
import com.bbva.javaparteii.Service.CuentaService;
import com.bbva.javaparteii.Service.MovimientoService;
import com.bbva.javaparteii.Service.ResultadoCambio;
import com.bbva.javaparteii.Service.impl.ResultadoCambioImpl;
import com.bbva.javaparteii.model.Cuenta;
import com.bbva.javaparteii.model.Movimiento;
import com.bbva.javaparteii.model.movimiento.CompraMonedaExtranjera;
import com.bbva.javaparteii.model.movimiento.VentaMonedaExtranjera;
import com.bbva.javaparteii.rest.request.AgregarCotitularRequest;
import com.bbva.javaparteii.rest.request.CrearCuentaRequest;
import com.bbva.javaparteii.rest.request.CurrencyRequest;
import com.bbva.javaparteii.rest.request.TransferenciaRequest;
import com.bbva.javaparteii.util.currencyconverter.CurrencyConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/cuentas")
public class CuentaController {

    private final CuentaService cuentaService;
    private final MovimientoService movimientoService;



    @Autowired
    public CuentaController(CuentaService cuentaService,
                            MovimientoService movimientoService

    ) {
        this.cuentaService = cuentaService;
        this.movimientoService = movimientoService;

    }

    @PostMapping
    public ResponseEntity<Cuenta> crearCuenta(@RequestBody CrearCuentaRequest request) {
        Cuenta nuevaCuenta = cuentaService.guardarCuenta(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(nuevaCuenta);
    }

    @PostMapping("/agregarCotitular")
    public ResponseEntity<Cuenta> agregarCotitular(@RequestBody AgregarCotitularRequest request) {
        Cuenta nuevaCuenta = cuentaService.agregarCotitular(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(nuevaCuenta);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Cuenta> obtenerCuentaPorId(@PathVariable Long id) {
        Cuenta cuenta = cuentaService.obtenerCuentaPorId(id);
        if (cuenta != null) {
            return ResponseEntity.ok(cuenta);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping
    public ResponseEntity<Set<Cuenta>> obtenerTodasLasCuentas() {
        Set<Cuenta> cuentas =  cuentaService.obtenerTodasLasCuentas();
        return ResponseEntity.ok(cuentas);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> eliminarCuenta(@PathVariable Long id) {
        Cuenta cuenta = cuentaService.obtenerCuentaPorId(id);
        if (cuenta != null) {
            cuentaService.eliminarCuenta(cuenta);
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/transferir")
    public ResponseEntity<Movimiento> transferir(@RequestBody TransferenciaRequest request){

        movimientoService.realizarTransferencia(request);

        return ResponseEntity.ok(null);
    }

    @GetMapping("/movimientos/{id}")
    public ResponseEntity<Set<Movimiento>> getMovimientos(@PathVariable Long id){

        Set<Movimiento> response =  movimientoService.obtenerTodosLosMovimientosPorIdTitular(); //TODO agregar el ID

        return ResponseEntity.ok(response); //TODO ESTO NO ESTA BIEN CAMBIAR POR OBTENER POR ID CUENTA O ALGO ASI
    }

    @PostMapping("/vender")
    public ResponseEntity<Object> venderMonedaExtranjera(@RequestBody CurrencyRequest request){


        cuentaService.vender(request);


        return ResponseEntity.ok("//TODO cambiar esto por otra cosa, tal vez devolver el movimiento?");
    }

}