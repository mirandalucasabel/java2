package com.bbva.javaparteii.rest.response;

import com.bbva.javaparteii.model.Cliente;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ClienteResponse {

    //TODO aca deberia ir un dto y no devolver el modelo
    Cliente Cliente;
    List<String> mensajes = new ArrayList<>();
}
