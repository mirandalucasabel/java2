package com.bbva.javaparteii.rest.request;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CrearCuentaRequest {
    private BigDecimal saldoInicial;
    private Long titularId;
    private String moneda;
}
