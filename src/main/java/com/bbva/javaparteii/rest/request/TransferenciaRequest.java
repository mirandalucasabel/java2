package com.bbva.javaparteii.rest.request;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TransferenciaRequest {
    private BigDecimal monto;
    private Long idCuentaOrigen;
    private Long idCuentaDestino;
}
