package com.bbva.javaparteii.rest.request;

import lombok.Data;

@Data
public class AgregarCotitularRequest {

    Long idCuenta;
    Long idCotitular;
}
