package com.bbva.javaparteii.rest.request;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CurrencyRequest {
     Double monto;
     Long idCuentaOrigen;
     Long idCuentaDestino;
}
