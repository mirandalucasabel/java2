package com.bbva.javaparteii.dao;

import java.util.List;

public interface Dao<T> {
    void save(T entidad);
    T getById(Long id);
    List<T> all();
    void delete(T entidad);
}