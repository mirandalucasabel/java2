package com.bbva.javaparteii.dao;

import com.bbva.javaparteii.model.Movimiento;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovimientoDao extends JpaRepository<Movimiento, Long> {
}
