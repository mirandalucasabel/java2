package com.bbva.javaparteii.dao;

import com.bbva.javaparteii.model.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {
    // Método de búsqueda por nombre
    Set<Cliente> findByNombre(String nombre);
}