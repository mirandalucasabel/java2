package com.bbva.javaparteii.dao;

import com.bbva.javaparteii.model.Cuenta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CuentaDao extends JpaRepository<Cuenta, Long> {

    //TODO agregar las queries que faltan
}
