package com.bbva.javaparteii.dao.impl;

import com.bbva.javaparteii.dao.ClienteDao;
import com.bbva.javaparteii.model.Cliente;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class ClienteDaoImpl implements ClienteDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public void save(Cliente cliente) {
        entityManager.persist(cliente);
    }

    @Override
    public Cliente getById(Long id) {
        return entityManager.find(Cliente.class, id);
    }

    @Override
    public List<Cliente> all() {
        return entityManager.createQuery("SELECT c FROM Cliente c", Cliente.class).getResultList();
    }

    @Override
    public void delete(Cliente cliente) {
        entityManager.remove(cliente);
    }

}
