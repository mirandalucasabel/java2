package com.bbva.javaparteii.util.currencyconverter;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Getter
public class Info {
    @JsonProperty("timestamp")
    private String timestamp;

    @JsonProperty("quote")
    private Double quote;


    public String getTimestamp() {
        return timestamp;
    }

    public Double getQuote() {
        return quote;
    }


}