package com.bbva.javaparteii.util.currencyconverter;

import com.bbva.javaparteii.rest.request.CurrencyRequest;
import lombok.Data;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.net.URI;

@Data
public class CurrencyConverter {
    public  ApiResponse getCurrencyChangeAmount(String monedaOrigen, String monedaDestino, Double monto) {
        String apiKey = "---------";


       // String origen = ; //"GBP";
        //String destino = ; //"USD";
        String amount = monto.toString() ; //String.valueOf(19);
        String apiUrl = "https://api.apilayer.com/currency_data/convert?to=" +
                monedaOrigen +
                "&from=" +
                monedaDestino +
                "&amount=" +
                amount;


        RestTemplate restTemplate = new RestTemplate();


        HttpHeaders headers = new HttpHeaders();
        headers.set("apikey", apiKey);


        RequestEntity<?> requestEntity = new RequestEntity<>(headers, HttpMethod.GET, URI.create(apiUrl));


        ApiResponse response = restTemplate.exchange(requestEntity, ApiResponse.class).getBody();


        System.out.println("Amount: " + response.getQuery().getAmount());
        System.out.println("Result: " + response.getResult());

        return response;
    }
}
