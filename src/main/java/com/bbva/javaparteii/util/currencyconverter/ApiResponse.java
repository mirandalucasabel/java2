package com.bbva.javaparteii.util.currencyconverter;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class  ApiResponse {
    @JsonProperty("success")
    private boolean success;

    @JsonProperty("result")
    private double result;

    @JsonProperty("query")
    private Query query;

    @JsonProperty("info")
    private Info info;

    public boolean isSuccess() {
        return success;
    }

    public double getResult() {
        return result;
    }

    public Query getQuery() {
        return query;
    }

    public Info getInfo() {
        return info;
    }
}