package com.bbva.javaparteii;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaparteiiApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaparteiiApplication.class, args);
	}

}
